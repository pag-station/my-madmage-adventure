##@ Easy

.PHONY: help
help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_\/.~-]+:.*?##/ { printf "  \033[36m%-50s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


target/release/madman: target/pkg-release/adventure
	cargo build --release

target/debug/madman: target/pkg-debug/adventure
	cargo build

.ONESHELL:
target/pkg-release/adventure:
	wasm-pack build $(shell basename $@) --target web --release --out-dir $$PWD/$(shell dirname $@)

.ONESHELL:
target/pkg-debug/adventure:
	wasm-pack build $(shell basename $@) --target web --dev --out-dir $$PWD/$(shell dirname $@)

.PHONY: web
web: target/release/madman
	docker compose up -d --build web
