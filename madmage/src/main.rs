use rocket::response::content;

#[macro_use]
extern crate rocket;

#[get("/adventure.js")]
fn js_bundle() -> content::RawJavaScript<&'static str> {
    if cfg!(debug_assertions) {
        content::RawJavaScript(include_str!("../../target/pkg-debug/adventure.js"))
    } else {
        content::RawJavaScript(include_str!("../../target/pkg-release/adventure.js"))
    }
}

#[get("/adventure_bg.wasm")]
fn wasm_bundle() -> content::RawJavaScript<&'static [u8]> {
    if cfg!(debug_assertions) {
        content::RawJavaScript(include_bytes!("../../target/pkg-debug/adventure_bg.wasm"))
    } else {
        content::RawJavaScript(include_bytes!("../../target/pkg-release/adventure_bg.wasm"))
    }
}

#[get("/")]
fn index() -> content::RawHtml<&'static str> {
    content::RawHtml(include_str!("../../assets/index.html"))
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index, js_bundle, wasm_bundle])
}
